# language: id
Fitur: Buat project baru
  Dalam rangka membuat project baru
  Sebagai pengguna yang telah terdaftar pada sistem
  Saya akan membuat project baru


  Skenario: Membuat Project baru dengan data yang benar
    Dengan telah login sebagai "ted" dengan password "rahasia"
    Ketika saya meng-klik link "Create"
    Maka saya akan melihat form dengan legend "Create a new project - Base Info"
    Dan sayapun mengisikan data project dibawah ini pada form project tersebut:
      | name                | value                         |
      | project-name        | Melon Project                 |
      | project-identifier  | melon                         |
      | project-description | Deskripsi dari melon project  |
      | project-managers    | ted,barney                    |
      | project-members     | marshall,lily                 |

    Ketika saya menekan tombol "Next"
    Maka saya akan melihat form dengan legend "Create a new project - Repository"
    Dan sayapun mengisikan data informasi repository berikut ini:
      | name                | value                             |
      | repository-scm      | Git                               |
      | repository-address  | git@github.com/melon/melon.git    |
    Ketika saya menekan tombol "Next"
    Maka saya akan melihat form dengan legend "Create a new project - Server Testing"
    Dan sayapun mengisikan data informasi server testing berikut ini:
      | name                       | value         |
      | server-host                | 192.168.5.2   |
      | server-port                | 22            |
      | server-superuser_password  | root          |
      | server-user_login          | pumpkin       |
      | server-user_password       | pumpkin       |

    Ketika saya menekan tombol "Finish"
    Maka saya akan melihat text "Create project successful!"
    Ketika saya meng-klik link "Go to project"
    Maka saya akan menuju halaman overview dari project "melon"








