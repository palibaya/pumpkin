from behave import given, when, then

@given(u'telah login sebagai "{username}" dengan password "{password}"')
def impl(context, username, password):
    br = context.browser
    br.get(context.browser_url('/accounts/login/'))
    br.find_element_by_name('username').send_keys(username)
    br.find_element_by_name('password').send_keys(password)
    br.find_element_by_tag_name('form').submit()
    assert context.browser.title == 'Pumpkin - Home Page'

@when(u'Saya meng-klik link "{label}"')
def impl(context, label):
    context.browser.find_element_by_link_text(label).click()


@then(u'saya akan melihat form dengan legend "{label}"')
def impl(context, label):
    soup = context.parse_soup()

@then(u'sayapun mengisikan data project dibawah ini pada form project tersebut')
def impl(context):
    br = context.browser
    data = dict((row['name'], row['value']) for row in context.table)
    br.find_element_by_name('project-name').send_keys(data['project-name'])
    br.find_element_by_name('project-identifier').send_keys(data['project-identifier'])
    br.find_element_by_name('project-description').send_keys(data['project-description'])

    managers= data['project-managers'].split(',')
    for m in managers:
        br.find_element_by_xpath('//select[@name="project-managers"]/option[text()="%s"]' % m).click()

    members = data['project-members'].split(',')
    for m in members:
        br.find_element_by_xpath('//select[@name="project-members"]/option[text()="%s"]' % m).click()


@when(u'saya menekan tombol "{label}"')
def impl(context, label):
    context.browser.find_element_by_xpath('//button[text()="%s"]' % label).click()

@then(u'sayapun mengisikan data informasi repository berikut ini')
def impl(context):
    data = dict((row['name'], row['value']) for row in context.table)
    br = context.browser
    br.find_element_by_xpath('//select[@name="repository-scm"]/option[text()="Git"]').click()
    br.find_element_by_name('repository-address').send_keys(data['repository-address'])

@then(u'sayapun mengisikan data informasi server testing berikut ini')
def impl(context):
    for row in context.table:
        context.browser.find_element_by_name(row['name']).send_keys(row['value'])

@then(u'saya akan melihat text "{text}"')
def impl(context, text):
    soup = context.parse_soup()
    assert soup.find(text=text) == text

@then(u'saya akan menuju halaman overview dari project "{identifier}"')
def impl(context, identifier):
    print context.browser.current_url
    print context.browser_url('/project/%s' % identifier)
    assert context.browser.current_url == context.browser_url('/project/%s' % identifier)
