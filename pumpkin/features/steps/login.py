from behave import given, when, then


@given(u'memiliki informasi login user')
def impl(context):
    pass


@when(u'saya membuka halaman dengan session baru')
def impl(context):
    br = context.browser
    br.get(context.browser_url('/'))

@then(u'saya akan diarahkan ke halaman login')
def impl(context):
    assert context.browser.current_url.startswith(context.browser_url('/accounts/login'))

@when(u'mengisikan username "{username}"')
def impl(context, username):
    context.browser.find_element_by_name('username').send_keys(username)

@when(u'mengisikan password "{password}"')
def impl(context, password):
    context.browser.find_element_by_name('password').send_keys(password)

@when(u'melakukan submit form')
def impl(context):
    context.browser.find_element_by_tag_name('form').submit()

@then(u'Saya akan menuju pada halaman utama')
def impl(context):
    assert context.browser.title == 'Pumpkin - Home Page'

@then(u'di halaman utama saya akan melihat label website yang bertulisakan "pumpkin"')
def impl(context):
    soup = context.parse_soup()
    assert soup.find('a', text='pumpkin').text.lower() == 'pumpkin'

@then(u'Saya akan kembali ke halaman login')
def impl(context):
    assert context.browser.current_url.startswith(context.browser_url('/accounts/login'))

