# language: id
Fitur: Login
  Dalam rangka untuk melakukan validasi user
  Sebagai user
  Saya ingin login ke dalam sistem

  Skenario: Login Sukses
    Dengan memiliki informasi login user
    Ketika saya membuka halaman dengan session baru
    Maka saya akan diarahkan ke halaman login
    Ketika mengisikan username "ted"
    Dan mengisikan password "rahasia"
    Dan melakukan submit form
    Maka Saya akan menuju pada halaman utama
    Dan di halaman utama saya akan melihat label website yang bertulisakan "pumpkin"

  Skenario: Login gagal
    Dengan memiliki informasi login user
    Ketika saya membuka halaman dengan session baru
    Maka saya akan diarahkan ke halaman login
    Ketika mengisikan username "ted"
    Dan mengisikan password "salah"
    Dan melakukan submit form
    Maka saya akan diarahkan ke halaman login
