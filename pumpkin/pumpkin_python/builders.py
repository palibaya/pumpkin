from pumpkin.builders import BashBuilder

class VirtualenvBashBuilder(BashBuilder):
    pre_command = '''
        if [ ! -d $PROJECT_WORKSPACE ]; then
            %(scm_clone)s
        fi
        cd $PROJECT_WORKSPACE
        %(scm_fetch)s
        %(scm_create_branch)s

        export VIRTUAL_ENV=${PROJECT_WORKSPACE}_virtualenv
        if [ ! -d $VIRTUAL_ENV ]; then
            virtualenv $VIRTUAL_ENV;
        fi
        source $VIRTUAL_ENV/bin/activate
        '''
