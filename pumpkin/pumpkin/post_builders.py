from pumpkin.builders import BaseBuilder

class BasePostBuilder(BaseBuilder):

    input_name = 'postpostbuild_content'

    def save_postbuild(self, job, postbuild_id, postbuild_content,
                       index):
        try:
            postbuild = PostBuild.objects.get(id=postbuild_id)
        except PostBuild.DoesNotExist:
            postbuild = PostBuild()
            postbuild.postbuilder = self.postbuilder
            postbuild.job = job
        postbuild.sequence = index + 1
        postbuild.content = postbuild_content
        postbuild.save()
        return postbuild


class EMailFeedbackBuilder(BasePostBuilder):

    input_label = 'Email Addresses'


    def run(self):
        print 'Email Feedback'
        ''' implemented method'''
        pass
