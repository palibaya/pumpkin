from datetime import datetime

from django.conf import settings
from django.template import Template, Context, loader

import pytz
from ansi2html import Ansi2HTMLConverter

from pumpkin.models import BuildLog, Build
from pumpkin.ssh import SSHClient
from pumpkin.tools import get_obj


current_tz =  pytz.timezone(settings.TIME_ZONE)

class BaseBuilder(object):
    '''
    class sebagai backend dalam melakukan running Job
    '''

    template = 'builders/builder.html'
    input_label = 'Content'
    input_name = 'build_content'
    html_help = 'Help content'

    def __init__(self, builder=None, build=None, job_log=None):
        self.builder = builder
        self.build = build
        self.job_log = job_log


    def setup(self):
        raise Exception("Unimplentation methods")

    def run(self):
        raise Exception("Unimplentation methods")

    def get_log(self):
        raise Exception("Unimplentation methods")

    def get_name(self):
        return self.__class__.__name__.lower()

    def get_builder_id(self):
        if self.builder is not None:
            return self.builder.id
        return 0

    def get_builder_name(self):
        if self.builder is not None:
            return self.builder.name
        return ''

    def get_build_id(self):
        if self.build is not None:
            return self.build.id
        return 0

    def get_build_content(self):
        if self.build is not None:
            return self.build.content
        return ''

    def get_template(self):
        c = Context({'input_label': self.input_label,
                    'input_name': self.input_name,
                    'input_value': self.get_build_content(),
                    'name': self.get_name(),
                    'build_id': self.get_build_id(),
                    'builder_name': self.get_builder_name(),
                    'builder_id': self.get_builder_id(),
                    'html_help': self.html_help,
                    })
        template = loader.get_template(self.template)
        return template.render(c)

    def save_build(self, job, build_id, build_content, index):
        try:
            build = Build.objects.get(id=build_id)
        except Build.DoesNotExist:
            build = Build()
            build.builder = self.builder
            build.job = job
        build.sequence = index + 1
        build.content = build_content
        build.save()
        return build

    def pre_run(self):
        pass

    def build_run(self):
        '''
        method yang dieksekusi oleh Build.run
        '''
        self.pre_run()
        self.run()
        self.post_run()

    def post_run(self):
        pass


class BashBuilder(BaseBuilder):

    html_help = '''
    <h5>Available Variable:</h5>
    <ul>
        <li><strong>$PROJECT_WORKSPACE</strong>: root of project</li>
        <li><strong>$PROJECT_ID</strong>: project identifier</li>
    </ul>
    '''

    input_label = 'Bash Command'

    pre_command = '''
        if [ ! -d $PROJECT_WORKSPACE ]; then
            %(scm_clone)s
        fi
        cd $PROJECT_WORKSPACE
        %(scm_fetch)s
        %(scm_create_branch)s
        '''

    post_command = '''
        %(scm_clear_branch)s
        '''

    def __init__(self, *args, **kwargs):
        super(BashBuilder, self).__init__(*args, **kwargs)

    def setup(self):
        self.client = SSHClient(self.build.job.project.server)

    def _create_log(self):
        self.log = BuildLog()
        self.log.build = self.build
        self.log.sequence = self.build.sequence
        self.log.job = self.build.job
        self.log.job_log = self.job_log
        self.log.content = self.build.content
        self.log.begin = current_tz.localize(datetime.now())

    def _save_log(self):
        if len(self.error_list) > 0:
            self.log.status = 'failure'
        else:
            self.log.status = 'success'
        self.log.output = ''.join(self.output_list)
        self.log.error = ''.join(self.error_list)
        self.log.end = current_tz.localize(datetime.now())
        conv = Ansi2HTMLConverter()
        self.log.output_html = conv.convert(self.log.output, full=False)
        self.log.error_html = conv.convert(self.log.error, full=False)
        self.log.save()

    def get_log(self):
        if not hasattr(self, 'log'):
            raise Exception('Builder has never run')
        return self.log


    def parse_input(self, querydict):
        self.build.content = querydict

    def pre_run(self):
        scm_runner = self.build.job.project.repository.get_runner()
        branch_name = self.build.job.repository_branch.name
        self.pre_command = self.pre_command % {
            'scm_clone': scm_runner.clone(),
            'scm_fetch': scm_runner.fetch(),
            'scm_create_branch': scm_runner.create_branch(branch_name),
        }
        self.command = self.build.content

        self.post_command = self.post_command % {
            'scm_clear_branch': scm_runner.clear_branch(branch_name),
        }

        if self.build.job.clean_workspace:
            self.pre_command = '''
                rm -rf $PROJECT_WORKSPACE
            ''' + self.pre_command
            self.post_command = self.post_command + '''
                cd $HOME
                rm -rf $PROJECT_WORKSPACE
            '''
        self._create_log()


    def post_run(self):
        self._save_log();

    def run(self):
        #connect ssh
        self.client.set_params(self.build.job.project.get_params())
        self.client.connect()
        #send command
        self.client.add_command(self.pre_command)
        self.client.add_command(self.command)
        self.client.add_command(self.post_command)
        stdin, stdout, stderr = self.client.execute()
        #read output
        self.output_list = stdout.readlines()
        self.error_list = stderr.readlines()
        #close ssh session
        self.client.close()


class PythonClassBuilder(BaseBuilder):

    input_label = 'Class Name'

    def run(self):
        ''' implemented method'''
        pass

class PythonFunctionBuilder(BaseBuilder):

    def run(self):
        ''' implemented method'''
        pass


