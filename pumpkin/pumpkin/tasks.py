from celery import task
from pumpkin.models import Job

@task
def add(x, y):
    return x + y

@task
def project_setup(project):
    project.setup()

@task
def run_job(job):
    job.set_running()
    job.run()
    return job.last_run()


@task
def run_job_by_id(job_id):
    '''
    hanya akan menjalankan job ketika ada perubahan pada source code
    '''
    job = Job.objects.get(id=job_id)
    if job.is_ready():
        job.run()
        return job.last_run()



@task
def run_job_by_id_check(job_id):
    '''
    hanya akan menjalankan job ketika ada perubahan pada source code
    '''
    job = Job.objects.get(id=job_id)
    if job.is_ready():
        if job.repository_branch.is_remote_updated():
            job.run()
            return job.last_run()
