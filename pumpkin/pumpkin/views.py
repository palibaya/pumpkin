from django.shortcuts import get_object_or_404, render, redirect
from django.contrib.auth.decorators import login_required

from pumpkin import models, forms, tasks

@login_required
def home(request):

    return render(request, 'home.html', {})

@login_required
def project(request, identifier):
    project = models.Project.objects.get(identifier=identifier)
    return render(request, 'project/home.html', {
        'project': project
    })

@login_required
def project_jobs(request, identifier):
    project = models.Project.objects.get(identifier=identifier)
    return render(request, 'project/jobs.html', {
        'project': project,
        'jobs': project.jobs.all(),
    })

@login_required
def project_job_create(request, identifier):

    project = models.Project.objects.get(identifier=identifier)
    if request.method == "POST":
        job_form = forms.NewJobForm(request.POST)
        if job_form.is_valid():

            job = job_form.save(commit=False)
            job.project = project
            job.save()
            return redirect('pumpkin_project_job_configure',
                            identifier=identifier, job_id=job.id)
    else:
        job_form = forms.NewJobForm()

    return render(request, 'project/job/create.html', {
        'project': project,
        'job_form': job_form,
    })

@login_required
def project_job_run(request, identifier, job_id):
    job = models.Job.objects.get(id=job_id)
    if job.is_ready():
        job.set_queuing()
        tasks.run_job.delay(job)
    else:
        print 'Job is running or queuing '
    return redirect('pumpkin_project_jobs', identifier=identifier)


@login_required
def project_job_logs(request, identifier, job_id):
    job = models.Job.objects.get(id=job_id)
    logs = job.logs.order_by('-begin').all()
    return render(request, 'project/job/logs.html', {
        'project': job.project,
        'job': job,
        'logs': logs
    })

@login_required
def project_job_configure(request, identifier, job_id):
    project = models.Project.objects.get(identifier=identifier)
    project.repository.update_branches()
    job = models.Job.objects.get(id=job_id)
    triggers = models.Trigger.objects.all()
    builders = models.Builder.objects.all()
    postbuilders = models.PostBuilder.objects.all()
    if request.method == "POST":
        job_form = forms.JobForm(request.POST,
                                 instance=job,
                                 project_id=project.id)
        if job_form.is_valid():
            job = job_form.save()
            job.process_trigger_form(request.POST)

            #== BUILD SAVE ==
            build_ids = request.POST.getlist('build_id')
            build_contents = request.POST.getlist('build_content')
            builder_ids = request.POST.getlist('builder_id')
            indexes = range(len(builder_ids))
            new_build_ids = []
            #iter build
            for build_id, build_content, builder_id, index in \
                zip(build_ids, build_contents, builder_ids, indexes):

                builder_obj = models.Builder.objects.get(id=builder_id)\
                                .get_instance()
                build = builder_obj.save_build(job, int(build_id),
                                               build_content, index)
                new_build_ids.append(build.id)
            #delete build
            models.Build.objects.filter(job_id=job_id)\
                        .exclude(id__in=new_build_ids).delete()
            #== END BUILD SAVE ==

            #== POST BUILD SAVE==
            postbuild_ids = request.POST.getlist('postbuild_id')
            postbuild_contents = request.POST.getlist('postbuild_content')
            postbuilder_ids = request.POST.getlist('postbuilder_id')
            indexes = range(len(postbuilder_ids))
            new_postbuild_ids = []
            #iter postbuild
            for postbuild_id, postbuild_content, postbuilder_id, index\
                in zip(postbuild_ids, postbuild_contents,
                       postbuilder_ids, indexes):
                postbuilder_obj = models.PostBuilder.objects\
                                        .get(id=postbuilder_id)\
                                        .get_instance()
                postbuild = postbuilder_obj.save_build(job,
                                                       int(postbuild_id),
                                                       postbuild_content,
                                                       index)
                new_postbuild_ids.append(postbuild.id)
            #delete postbuild
            models.PostBuild.objects.filter(job_id=job_id)\
                  .exclude(id__in=new_postbuild_ids).delete()
            #== END POST BUILD SAVE==

    else:
        job_form = forms.JobForm(project_id=project.id, instance=job)

    return render(request, 'project/job/configure.html', {
        'project': project,
        'job': job,
        'job_form': job_form,
        'triggers': triggers,
        'builders': builders,
        'postbuilders': postbuilders,
    })

@login_required
def project_job_delete(request, identifier, job_id):
    job = models.Job.objects.get(id=job_id)
    job.delete()
    return redirect('pumpkin_project_jobs', identifier=identifier)

@login_required
def project_configure(request, identifier):
    project = models.Project.objects.get(identifier=identifier)
    if request.method == "POST":
        form = forms.ProjectForm(request.POST, instance=project)
        if form.is_valid():
            form.save()
    else:
        form = forms.ProjectForm(instance=project)
    return render(request, 'project/configure/base.html', {
        'project': project,
        'form': form,
    })

@login_required
def project_configure_repository(request, identifier):
    project = models.Project.objects.get(identifier=identifier)
    if request.method == "POST":
        form = forms.RepositoryForm(request.POST,
                                    instance=project.repository)
        if form.is_valid():
            form.save()
    else:
        form = forms.RepositoryForm(instance=project.repository)
    return render(request, 'project/configure/repository.html', {
        'project': project,
        'form': form,
    })


@login_required
def project_configure_server(request, identifier):
    project = models.Project.objects.get(identifier=identifier)
    if request.method == "POST":
        form = forms.ServerForm(request.POST, instance=project.server)
        if form.is_valid():
            form.save()
    else:
        form = forms.ServerForm(instance=project.server)
    return render(request, 'project/configure/server.html', {
        'project': project,
        'form': form,
    })
