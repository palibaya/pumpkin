# -*- coding: utf-8 -*-
from django import forms
from pumpkin import models

class NewProjectForm(forms.ModelForm):
    class Meta:
        model = models.Project
        fields = ('name', 'identifier', 'description',
                  'managers', 'members')

class NewRepositoryForm(forms.ModelForm):
    class Meta:
        model = models.Repository
        fields = ('scm', 'address')

class NewServerForm(forms.ModelForm):
    class Meta:
        model = models.Server
        fields = ('host', 'port', 'superuser_password',
                  'user_login', 'user_password')

class ProjectForm(forms.ModelForm):
    class Meta:
        model = models.Project
        fields = ('name', 'identifier', 'description')

class RepositoryForm(forms.ModelForm):
    class Meta:
        model = models.Repository
        fields = ('scm', 'address')


class ServerForm(forms.ModelForm):
    class Meta:
        model = models.Server
        fields = ('host', 'port', 'superuser_password',
                  'user_login', 'user_password')

class NewJobForm(forms.ModelForm):
    class Meta:
        model = models.Job
        fields = ('name', 'description')

class JobForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        project_id = kwargs.pop('project_id', None)
        super(JobForm, self).__init__(*args, **kwargs)
        if project_id:
            project = models.Project.objects.get(id=project_id)
            queryset = project.repository.branches.all()
            self.fields['repository_branch'].queryset = queryset

    class Meta:
        model = models.Job
        fields = ('name', 'description', 'repository_branch',
                  'clean_workspace')


class JobTriggerForm(forms.ModelForm):
    class Meta:
        model = models.JobTrigger
        fields = ('job','trigger', 'content', 'serialize_data')



