from django.utils import simplejson
from django.template import Context, loader
from django.db import models
from django import forms
from djcelery.models import (IntervalSchedule, PeriodicTask,
                             CrontabSchedule)

from pumpkin.models import Trigger, JobTrigger
from pumpkin.forms import JobTriggerForm

class BaseTriggerRunner(object):

    template = 'triggers/base.html'
    form_class = JobTriggerForm
    form_prefix = 'job_trigger'

    def __init__(self, trigger, job=None, job_trigger=None):
        self.trigger = trigger
        self.job = job
        self.job_trigger = job_trigger
        self.cleaned_data = None

    def setup(self):
        raise Exception("Unimplentation methods")

    def setdown(self):
        raise Exception("Unimplentation methods")

    def make_serializable(self, data):
        raw = {}
        for k,v in data.iteritems():
            if isinstance(v, models.Model):
                raw[k] = v.id
            else:
                raw[k] = v
        return raw


    def get_initial_data(self):
        initial = {
            'trigger_id': self.trigger.id,
            'job_id': self.job.id
        }
        if self.job_trigger is None:
            return initial
        initial.update({
            'id': self.job_trigger.id,
        })
        initial.update(simplejson.loads(self.job_trigger.serialize_data))
        return initial

    def get_unbound_form(self):
        return self.form_class(prefix=self.form_prefix,
                               initial=self.get_initial_data())

    def get_form(self):
        return self.get_unbound_form()

    def process_form(self, query_dict):
        form = self.form_class(query_dict, prefix=self.form_prefix,
                               initial=self.get_initial_data())
        if form.is_valid():
            serialize_data = simplejson.dumps(self.make_serializable(form.cleaned_data))
            defaults = {'serialize_data': serialize_data}
            jt, created = JobTrigger.objects\
                                    .get_or_create(trigger=self.trigger,
                                                   job=self.job,
                                                   defaults=defaults)
            if not created:
                jt.serialize_data = serialize_data
                jt.save()
            self.job_trigger = jt
            self.cleaned_data = form.cleaned_data
        else:
            print form.errors

    def get_html_form(self):
        c = Context({
            'form': self.get_form(),
            'trigger': self.trigger,
            'job_trigger': self.job_trigger,
        })
        template = loader.get_template(self.template)
        return template.render(c)


class PeriodicTriggerRunner(BaseTriggerRunner):

    class FormClass(forms.Form):
        id = forms.CharField(widget=forms.HiddenInput, required=False)
        job_id = forms.CharField(widget=forms.HiddenInput)
        trigger_id = forms.CharField(widget=forms.HiddenInput)
        interval = forms.ModelChoiceField(queryset=IntervalSchedule.\
                                          objects.all())
        only_changed = forms.BooleanField(required=False, initial=True, label="Execute only when repository has changed")

    form_class = FormClass
    form_prefix = 'job_trigger_periodic'

    def setup(self):
        if self.cleaned_data is not None:
            name = 'job_trigger_%s' % self.job_trigger.id
            task = 'pumpkin.tasks.run_job_by_id'
            if self.cleaned_data['only_changed']:
                task = 'pumpkin.tasks.run_job_by_id_check'
            defaults = {
                'task': task,
                'interval': self.cleaned_data['interval'],
                'args': str([self.job.id]),
            }
            pt, c = PeriodicTask.objects\
                                .get_or_create(name=name,
                                               defaults=defaults)
            if not c:
                pt.task = task
                pt.interval = self.cleaned_data['interval']
                pt.args = str([self.job.id])
                pt.save()

    def setdown(self):
        if self.job_trigger is not None:
            try:
                PeriodicTask.objects.get(name='job_trigger_%s' % \
                                     self.job_trigger.id).delete()
            except PeriodicTask.DoesNotExist:
                print 'PeriodicTask.DoesNotExist'
            self.job_trigger.delete()



class CronjobTriggerRunner(BaseTriggerRunner):


    class FormClass(forms.Form):
        id = forms.CharField(widget=forms.HiddenInput, required=False)
        job_id = forms.CharField(widget=forms.HiddenInput)
        trigger_id = forms.CharField(widget=forms.HiddenInput)
        cron = forms.CharField()
        only_changed = forms.BooleanField(required=False, label="Execute only when repository has changed")

    form_class = FormClass
    form_prefix = 'job_trigger_cronjob'


    def setup(self):
        if self.cleaned_data is not None:
            name = 'job_trigger_%s' % self.job_trigger.id
            task = 'pumpkin.tasks.run_job_by_id'
            if self.cleaned_data['only_changed']:
                task = 'pumpkin.tasks.run_job_by_id_check'

            cron = filter(lambda x: len(x) > 0,
                          self.cleaned_data['cron'].split(' '))
            if len(cron) != 5:
                print 'Cron tidak sesuai'
                return False
            minute, hour, dom, month, dow = cron
            cs = CrontabSchedule.objects.create(minute=minute,
                                                hour=hour,
                                                day_of_week=dow,
                                                day_of_month=dom,
                                                month_of_year=month)
            defaults = {
                'task': task,
                'crontab': cs,
                'args': str([self.job.id]),
            }
            pt, c = PeriodicTask.objects.get_or_create(name=name,
                                                       defaults=defaults)
            if not c:
                pt.task = task
                pt.crontab = cs
                pt.args = str([self.job.id])
                pt.save()



    def setdown(self):
        if self.job_trigger is not None:
            try:
                pt = PeriodicTask.objects.get(name='job_trigger_%s' % \
                                              self.job_trigger.id)
                pt.crontab.delete()
                pt.delete()
            except PeriodicTask.DoesNotExist:
                print 'PeriodicTask.DoesNotExist'
            self.job_trigger.delete()
