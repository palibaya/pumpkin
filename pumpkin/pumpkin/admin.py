# -*- coding: utf-8 -*-

from django.contrib import admin
from pumpkin import models

admin.site.register(models.Server)
admin.site.register(models.SCM)
admin.site.register(models.Project)
admin.site.register(models.Trigger)
admin.site.register(models.Builder)
admin.site.register(models.PostBuilder)

class RepositoryBranchInline(admin.TabularInline):
    model = models.RepositoryBranch

class RepositoryAdmin(admin.ModelAdmin):
    inlines = [RepositoryBranchInline]


admin.site.register(models.Repository, RepositoryAdmin)


class BuildInline(admin.TabularInline):
    model = models.Build

class JobTriggerInline(admin.TabularInline):
    model = models.JobTrigger

class JobAdmin(admin.ModelAdmin):
    inlines = [
        BuildInline,
        JobTriggerInline
    ]

admin.site.register(models.Job, JobAdmin)

class BuildLogInline(admin.TabularInline):
    model = models.BuildLog
    fields = ('content', 'output', 'error',
              'output_html', 'error_html','status')
    readonly_fields = ('status',)

class JobLogAdmin(admin.ModelAdmin):
    inlines = [
        BuildLogInline,
    ]

admin.site.register(models.JobLog, JobLogAdmin)

