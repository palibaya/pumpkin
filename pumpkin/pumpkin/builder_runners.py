from django.utils import simplejson
from django.template import Context, loader
from django.db import models
from django import forms

class BuilderForm(forms.Form):
    build_id = forms.CharField(widget=forms.HiddenInput)
    builder_id = forms.CharField(widget=forms.HiddenInput)
    content = forms.CharField(widget=forms.Textarea)


class BaseBuilderRunner(object):

    template = 'builder/base.html'
    form_class = BuilderForm
    form_prefix = 'builder'

    def __init__(self, builder, job=None, build=None):
        self.trigger = trigger
        self.job = job
        self.build = build
        self.cleaned_data = None

    def setdown(self):
        raise Exception("Unimplentation methods")

    def make_serializable(self, data):
        raw = {}
        for k,v in data.iteritems():
            if isinstance(v, models.Model):
                raw[k] = v.id
            else:
                raw[k] = v
        return raw


    def get_initial_data(self):
        initial = {
            'trigger_id': self.trigger.id,
            'job_id': self.job.id
        }
        if self.job_trigger is None:
            return initial
        initial.update({
            'id': self.job_trigger.id,
        })
        initial.update(simplejson.loads(self.job_trigger.serialize_data))
        return initial

    def get_unbound_form(self):
        return self.form_class(prefix=self.form_prefix,
                               initial=self.get_initial_data())

    def get_form(self):
        return self.get_unbound_form()

    def process_form(self, query_dict):
        form = self.form_class(query_dict, prefix=self.form_prefix,
                               initial=self.get_initial_data())
        if form.is_valid():
            serialize_data = simplejson.dumps(self.make_serializable(form.cleaned_data))
            defaults = {'serialize_data': serialize_data}
            jt, created = JobTrigger.objects\
                                    .get_or_create(trigger=self.trigger,
                                                   job=self.job,
                                                   defaults=defaults)
            if not created:
                jt.serialize_data = serialize_data
                jt.save()
            self.job_trigger = jt
            self.cleaned_data = form.cleaned_data
        else:
            print form.errors

    def get_html_form(self):
        c = Context({
            'form': self.get_form(),
            'trigger': self.trigger,
            'job_trigger': self.job_trigger,
        })
        template = loader.get_template(self.template)
        return template.render(c)
