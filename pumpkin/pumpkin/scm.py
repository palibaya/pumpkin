import subprocess

from pumpkin.models import RepositoryBranch

class BaseSCMRunner(object):

    def __init__(self, repository):
        self.repository = repository

    def clone(self):
        raise Exception("Unimplentation methods")

    def create_branch(self, branch):
        raise Exception("Unimplentation methods")

    def clear_branch(self, branch):
        raise Exception("Unimplentation methods")

    def update_branches(self, branch):
        '''
        method ini untuk mengupdate data pada model
        RepositoryBranch terhadap sebuah project
        '''
        raise Exception("Unimplentation methods")


class GitSCMRunner(BaseSCMRunner):


    def clone(self):
        return 'git clone %s --quiet' % self.repository.address

    def fetch(self):
        return 'git fetch --quiet'

    def create_branch(self, branch):
        return 'git checkout -b tmp_%s origin/%s --quiet' % (branch, branch)

    def clear_branch(self, branch):
        return '''
            git checkout master --quiet
            git branch -D tmp_%s
            ''' % (branch)

    def get_updated_branches(self):
        process = subprocess.Popen('git ls-remote -h %s' % self.repository.address,
                                   shell=True, stdout=subprocess.PIPE,
                                   stderr=subprocess.STDOUT)
        names = []
        updated_names = []
        for line in process.stdout.readlines():
            ref, name = line.split('\t')
            name = name.strip('\n').split('/')[-1]
            names.append(name)
            branch, created = RepositoryBranch.objects.get_or_create(name=name,\
                                    repository=self.repository,
                                    defaults={'last_ref': ref})
            #update if exist
            if not created:
                if branch.last_ref != ref:
                    updated_names.append(branch.name)
                    branch.last_ref = ref
                branch.save()
        #delete if not exist
        RepositoryBranch.objects.filter(repository=self.repository)\
              .exclude(name__in=names)\
              .delete()
        return updated_names

    def is_remote_updated(self, branch):
        branches = self.get_updated_branches()
        return branch.name in branches

    def update_branches(self):
        self.get_updated_branches()

