import os
import os.path
from datetime import datetime

from django.conf import settings
from django.db import models
from django.core.urlresolvers import reverse
from django.db.models.loading import get_model
from django.contrib.auth.models import User

import pytz

from pumpkin.tools import get_obj

current_tz =  pytz.timezone(settings.TIME_ZONE)


class BaseModel(models.Model):

    class Meta:
        abstract = True

    def __unicode__(self):
        if hasattr(self, 'name'):
            return self.name
        else:
            return '%s ID:%s' % (type(self).__name__, self.id)


class Server(BaseModel):
    name = models.CharField(max_length=255)
    host = models.CharField(max_length=32, verbose_name='Host Address')
    port = models.PositiveIntegerField(verbose_name='SSH Port',
                                       default=22)
    superuser_login = models.CharField(max_length=32,
                                       verbose_name='Root')
    superuser_password = models.CharField(max_length=255,
                                          verbose_name='Root Password')
    user_login = models.CharField(max_length=32,
                                  verbose_name='User Login')
    user_password = models.CharField(max_length=255,
                                     verbose_name='User Password')
    ssh_key_pub = models.TextField(blank=True, null=True)


class SCM(BaseModel):
    name = models.CharField(max_length=255)
    code = models.CharField(max_length=255)
    class_name = models.CharField(max_length=255)


class Repository(BaseModel):
    name = models.CharField(max_length=255)
    address = models.CharField(max_length=255,
                               verbose_name='Repository Address')
    scm = models.ForeignKey(SCM, related_name='+', default=1,
                            verbose_name='Repository SCM')

    def update_branches(self):
        self.get_runner().update_branches()


    def get_runner(self):
        if not hasattr(self, '_runner'):
            self._runner = get_obj(self.scm.class_name,
                                   {'repository': self})
        return self._runner



class RepositoryBranch(BaseModel):
    name = models.CharField(max_length=255)
    repository = models.ForeignKey(Repository, related_name='branches')
    last_ref = models.CharField(max_length=255)

    def is_remote_updated(self):
        return self.repository.get_runner().is_remote_updated(self)



class ProjectBase(BaseModel):
    """
    Model ini untuk menampung project yang akan dikelola
    """
    name = models.CharField(max_length=255, unique=True)
    identifier = models.SlugField(unique=True)
    description = models.TextField(blank=True)


    def get_params(self):
        params = {
            '%s_REPOSITORY_ADDRESS' % self.repository.scm.code: \
                    self.repository.address,
            'PROJECT_ID': '%s' % self.identifier,
            'PROJECT_WORKSPACE': self.get_workspace_path(),
        }
        #extra = dict([(p.key, p.value) for p in self.params.all()])
        #params.update(extra)
        return params

    def get_absolute_url(self):
        return reverse('pumpkin.views.project',
                       args=[str(self.identifier)])

    def get_workspace_path(self):
        return '$HOME/%s' % self.identifier.replace('-','_')

    class Meta:
        abstract = True


class Project(ProjectBase):
    managers = models.ManyToManyField(User,
                                      related_name='managered_projects')
    members = models.ManyToManyField(User, null=True, blank=True,
                                     related_name='membered_projects')

    server = models.OneToOneField(Server)
    repository = models.OneToOneField(Repository,
                                      related_name='project')

class ProjectTemplate(ProjectBase):
    pass


class ProjectBranch(BaseModel):
    name = models.CharField(max_length=255)
    project = models.ForeignKey(Project)


class ProjectParam(BaseModel):
    key = models.CharField(max_length=255)
    value = models.CharField(max_length=255)
    project = models.ForeignKey(Project, related_name='params')


class JobLog(BaseModel):
    STATUS_CHOICES = (
        ('success', 'Success'),
        ('failure','Failure'),
        ('partial', 'Partial'),
        #('queuing', 'Queuing'),
        #('running', 'Running'),
        #('terminate', 'Terminate'),
    )
    job = models.ForeignKey('Job', related_name='logs')
    begin = models.DateTimeField()
    end = models.DateTimeField(null=True, blank=True)
    status = models.CharField(max_length=16, choices=STATUS_CHOICES)
    #deprecated
    branch = models.ForeignKey(ProjectBranch, null=True, blank=True)


    def get_status(self):
        return self.status
        if self.status is not None and self.status != '':
            return self.status
        if os.path.exists(self.job.get_queue_file()):
            return 'queuing'
        if os.path.exists(self.job.get_run_file()):
            return 'running'
        return 'terminate'

    def duration(self):
        return self.end - self.begin

    def __unicode__(self):
        return 'Job Log #%s: %s' % (self.id, self.job)


class JobBase(BaseModel):
    name = models.CharField(max_length=255)

    def _create_log(self, branch):
        job_log = JobLog()
        job_log.job = self
        job_log.branch = branch
        job_log.begin = current_tz.localize(datetime.now())
        job_log.save()
        return job_log

    def _save_log(self, job_log, build_statuses):
        status_set = set(build_statuses)
        if len(status_set) == 1 and 'success' in status_set:
            job_log.status = 'success'
        elif len(status_set) == 1 and 'failure' in status_set:
            job_log.status = 'failure'
        else:
            job_log.status = 'partial'
        job_log.end = current_tz.localize(datetime.now())
        job_log.save()

    def run(self, branch=None):
        self.set_running()
        job_log = self._create_log(branch)
        build_statuses = []
        for build in self.builds.order_by('sequence'):
            build_statuses.append(build.run(job_log))
        self._save_log(job_log, build_statuses)
        self.clean_log_file()

    def get_last_status(self):
        if self.is_queuing():
            return 'queuing'
        if self.is_running():
            return 'running'
        last_run = self.last_run()
        if last_run is not None:
            return last_run.get_status()


    def get_last_build(self):
        lasts = self.builds.order_by('-sequence')
        if len(lasts) > 0:
            return lasts[0]

    def last_success(self):
        logs = self.logs.filter(status='success').order_by('-begin')
        if len(logs) > 0:
            return logs[0]


    def last_failure(self):
        logs =  self.logs.filter(status='failure').order_by('-begin')
        if len(logs) > 0:
            return logs[0]

    def last_run(self):
        if hasattr(self, '_last_run'):
            return self._last_run
        else:
            logs =  self.logs.order_by('-begin')
            if len(logs) > 0:
                self._last_run = logs[0]
            else:
                self._last_run = None
        return self._last_run

    def last_duration(self):
        if self.last_run() is not None:
            return self.last_run().duration()

    def get_trigger_html_forms(self):
        forms = [jt.get_html_form() for jt in self.job_triggers.all()]
        exist_ids = [jt.trigger.id for jt in self.job_triggers.all()]
        Trigger = get_model('pumpkin','Trigger')
        if Trigger.objects.count() == len(exist_ids):
            return forms
        triggers = Trigger.objects.exclude(id__in=exist_ids)
        forms += [t.get_runner(job=self).get_html_form() for t in triggers]
        return forms



    def get_initial_trigger_forms(self):
        forms = []
        exist_ids = []
        for jt in self.job_triggers.all():
            exist_ids.append(jt.trigger.id)
            forms.append(jt.get_form())
        Trigger = get_model('pumpkin','Trigger')
        if Trigger.objects.count() == len(exist_ids):
            return forms

        triggers = Trigger.objects.exclude(id__in=exist_ids)
        forms += [t.get_form() for t in triggers]
        return forms

    def process_trigger_form(self, querydict):
        Trigger = get_model('pumpkin','Trigger')
        enable_ids = [int(tid) for tid in \
                      querydict.getlist('trigger_enable_id')]

        for trigger in Trigger.objects.all(): #filter(id__in=enable_ids):
            trigger_runner = trigger.get_runner(self)
            trigger_runner.process_form(querydict)
            if trigger.id in enable_ids:
                trigger_runner.setup()
            else:
                trigger_runner.setdown()


    def get_queue_file(self):
        return '%s/%s.queue' % (settings.LOG_ROOT, self.id)

    def get_run_file(self):
        return '%s/%s.run' % (settings.LOG_ROOT, self.id)

    def is_queuing(self):
        return os.path.exists(self.get_queue_file())

    def is_running(self):
        return os.path.exists(self.get_run_file())

    def is_ready(self):
        return not self.is_queuing() and not self.is_running()

    def set_queuing(self):
        self.clean_log_file()
        open(self.get_queue_file(), 'w').close()

    def set_running(self):
        self.clean_log_file()
        open(self.get_run_file(), 'w').close()

    def clean_log_file(self):
        if os.path.exists(self.get_queue_file()):
            os.remove(self.get_queue_file())
        if os.path.exists(self.get_run_file()):
            os.remove(self.get_run_file())



    class Meta:
        abstract = True


class JobTemplate(JobBase):
    project_template = models.ForeignKey(ProjectTemplate,
                                         related_name='job_templates',
                                         blank=True, null=True)
    project = models.ForeignKey(Project, related_name='+',
                                blank=True, null=True)


class Job(JobBase):
    project = models.ForeignKey(Project, related_name='jobs')
    description = models.TextField(blank=True)
    repository_branch = models.ForeignKey(RepositoryBranch,
                                          blank=True, null=True,
                                          verbose_name='Branch')
    clean_workspace = models.BooleanField()


class Trigger(BaseModel):
    name = models.CharField(max_length=255)
    runner_class = models.CharField(max_length=255)

    def get_runner(self, job=None, job_trigger=None):
        #return get_obj(self.runner_class,{
            #'trigger': self,
            #'job': job,
            #'job_trigger': job_trigger
        #})
        if not hasattr(self, '_runner'):
            self._runner = get_obj(self.runner_class,
                                  {'trigger': self,
                                   'job': job,
                                   'job_trigger': job_trigger})
        return self._runner

    def get_form(self):
        return self.get_runner().get_form()

    def get_html_form(self):
        return self.get_runner().get_html_form()

    def get_inline_html_form(self):
        return self.get_html_form().replace('\n', '')


class JobTrigger(BaseModel):
    trigger = models.ForeignKey(Trigger)
    job = models.ForeignKey(Job, related_name='job_triggers')
    content = models.TextField(blank=True)
    serialize_data = models.TextField(blank=True)

    def get_runner(self):
        return self.trigger.get_runner(self.job, self)

    def get_form(self):
        return self.get_runner().get_form()

    def get_template(self):
        return self.get_runner().template

    def get_html_form(self):
        return self.get_runner().get_html_form()


class BaseBuilder(BaseModel):
    name = models.CharField(max_length=255)
    runner_class = models.CharField(max_length=255)

    #deprecated
    class_name = models.CharField(max_length=255)
    content = models.TextField(blank=True) # for template or other

    def get_instance(self):
        if not hasattr(self, '_instance'):
            self._instance = get_obj(self.class_name, {'builder': self})
        return self._instance

    def get_template(self):
        return self.get_instance().get_template()

    def get_inline_template(self):
        return self.get_template().replace('\n', '')

    class Meta:
        abstract = True
        ordering = ['id']


class Builder(BaseBuilder):
    pass


class PostBuilder(BaseBuilder):
    condition = models.TextField()


class BuildLog(BaseModel):
    STATUS_CHOICES = (
        ('success', 'Success'),
        ('failure','Failure'),
    )

    build = models.ForeignKey('Build', related_name='logs')
    job = models.ForeignKey(Job, related_name='build_logs')
    job_log = models.ForeignKey(JobLog, related_name='build_logs',
                                null=True)
    content = models.TextField() #for save command etc
    output = models.TextField(blank=True)
    error = models.TextField(blank=True)
    output_html = models.TextField(blank=True)
    error_html = models.TextField(blank=True)
    sequence = models.PositiveIntegerField(default=1)
    begin = models.DateTimeField()
    end = models.DateTimeField()
    status = models.CharField(max_length=16, choices=STATUS_CHOICES)
    branch = models.ForeignKey(ProjectBranch, null=True, blank=True)

    def content_amp(self):
        return self.content.replace('\n',' && ')

    class Meta:
        ordering = ['sequence']


class BaseBuild(BaseModel):
    """
    Model ini untuk menampung perintah yang akan dilakukan terhadap
    project yang dikelola
    """
    content = models.TextField()
    sequence = models.PositiveIntegerField(default=1)
    serialize_data = models.TextField(blank=True) # proccesed data

    def save(self):
        self.content = self.content.replace('\r\n', '\n')
        self.content = self.content.replace('\r', '\n')
        return super(BaseBuild, self).save()

    def get_builder_object(self):
        if not hasattr(self, '_builder_object'):
            self._builder_object = get_obj(self.builder.class_name,
                                           {'builder': self.builder,
                                            'build': self})
        return self._builder_object

    def run(self, job_log):
        builder = self.get_builder_object()
        builder.job_log = job_log
        builder.setup()
        builder.build_run()
        return builder.get_log().status

    def get_template(self):
        return self.get_builder_object().get_template()

    def save_data(self, querydict):
        self.get_builder_object().parse_input(querydict)

    class Meta:
        ordering = ['sequence']
        abstract = True


class BuildTemplate(BaseBuild):
    job = models.ForeignKey(JobTemplate, related_name='builds')
    builder = models.ForeignKey(Builder, related_name='+')


class PostBuildTemplate(BaseBuild):
    job = models.ForeignKey(JobTemplate, related_name='postbuilds')
    postbuilder = models.ForeignKey(PostBuilder, related_name='+')


class Build(BaseBuild):
    job = models.ForeignKey(Job, related_name='builds')
    builder = models.ForeignKey(Builder, related_name='+')


class PostBuild(BaseBuild):
    job = models.ForeignKey(Job, related_name='postbuilds')
    postbuilder = models.ForeignKey(PostBuilder, related_name='+')

